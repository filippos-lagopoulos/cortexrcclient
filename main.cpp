/*
	C ECHO client example using sockets
*/
#include <stdio.h>	//printf
#include <string.h>	//strlen
#include <sys/socket.h>	//socket
#include <arpa/inet.h>	//inet_addr
#include <unistd.h>
#include <string>
#include <random>
#include <signal.h>

#include "cortex-common/RemoteControlDefs.h"
#include "cortex-common/HashedString.h"

static volatile int keepRunning = 1;

void intHandler(int dummy) {
    keepRunning = 0;
}

int main(int argc , char *argv[])
{
	signal(SIGINT, intHandler);

	int startPresetIndex = 0;
	int endPresetIndex = 255;
	int loadFrequencyInMS = 1000;
	std::string setlistName = "";
	std::string ipaddr = "";
	bool randomIndex = false;

	std::string setlistsPrefix = "/media/p4/Presets/";

	if (argc < 3)
	{
		printf("Usage: %s [ipaddr] [setlistName] [startPresetIndex] [endPresetIndex] [loadFrequencyInMS] [randomIndex]\nipaddr and setlistName are required\n", argv[0]);
		return 1;
	}
	else
	{
		for (int i = 1; i < argc; i++)
		{
			if (i == 1)
			{
				ipaddr = argv[i];
				printf("Using server ipaddr %s\n", ipaddr.c_str());
			}
			if (i == 2)
			{
				setlistName = argv[i];
				printf("Using setlistName %s\n", setlistName.c_str());
			}
			else if (i == 3)
			{
				startPresetIndex = std::stoi(std::string(argv[i]));
				printf("Using startPresetIndex %d\n", startPresetIndex);
			}
			else if (i == 4)
			{
				endPresetIndex = std::stoi(std::string(argv[i]));
				printf("Using endPresetIndex %d\n", endPresetIndex);
			}
			else if (i == 5)
			{
				loadFrequencyInMS = std::stoi(std::string(argv[i]));
				printf("Using loadFrequencyInMS %d\n", loadFrequencyInMS);
			}
			else if (i == 6)
			{
				randomIndex = (std::string(argv[i]).compare("randomIndex") == 0);
				printf("Using randomIndex %d\n", randomIndex);
			}
		}
	}

	if (startPresetIndex >= endPresetIndex)
	{
		printf("startPresetIndex >= endPresetIndex\n");
		return 1;
	}

	if (loadFrequencyInMS <= 0)
	{
		printf("loadFrequencyInMS <= 0\n");
		return 1;
	}
	
	int sock;
	struct sockaddr_in server;
	char message[1000] , server_reply[2000];
	
	// Create socket
	sock = socket(AF_INET , SOCK_STREAM , 0);
	if (sock == -1)
	{
		printf("Could not create socket\n");
		return 1;
	}
	printf("Socket created\n");
	
	server.sin_addr.s_addr = inet_addr(ipaddr.c_str());
	server.sin_family = AF_INET;
	server.sin_port = htons(REMOTE_CONTROL_TCP_PORT);

	int yes = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) < 0)
    {
        perror("Error setting the SO_REUSEADDR");
		shutdown(sock, SHUT_RDWR);
		close(sock);
        return 1;
    }

    if (setsockopt(sock, SOL_SOCKET, SO_REUSEPORT, &yes, sizeof(int)) < 0)
    {
        perror("Error setting the SO_REUSEPORT");
		shutdown(sock, SHUT_RDWR);
		close(sock);
        return 1;
    }

	// Connect to remote server
	if (connect(sock, (struct sockaddr *)&server , sizeof(server)) < 0)
	{
		perror("connect failed. Error");
		shutdown(sock, SHUT_RDWR);
		close(sock);
		return 1;
	}

	printf("Connected\n");

    RemoteControlMessage msg;
    msg.type = static_cast<uint8_t>(RemoteControlMessageType::enPresetChange);

	auto setlistPath = setlistsPrefix + setlistName;
	uint32_t setlistHash = common::HashValue::calc(setlistPath.c_str());

    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_real_distribution<double> dist(static_cast<double>(startPresetIndex), static_cast<double>(endPresetIndex));

	// keep communicating with server
	while (keepRunning)
	{
		if (randomIndex)
		{
			RemoteControlPresetChangeMessage presetChangeMessage;
			presetChangeMessage.setlist = setlistHash;
			presetChangeMessage.presetIndex = static_cast<int>(dist(mt));

			memcpy(msg.buffer, &presetChangeMessage, sizeof(RemoteControlPresetChangeMessage));
			
			// send message
			if (send(sock, (void*)(&msg), sizeof(RemoteControlMessage) , 0) < 0)
			{
				printf("Send failed\n");
			}

			usleep(1000 * loadFrequencyInMS);
		}
		else
		{
			for (auto i = startPresetIndex; i < endPresetIndex; i++)
			{
				RemoteControlPresetChangeMessage presetChangeMessage;
				presetChangeMessage.setlist = setlistHash;
				presetChangeMessage.presetIndex = i;

				memcpy(msg.buffer, &presetChangeMessage, sizeof(RemoteControlPresetChangeMessage));
				
				// send message
				if (send(sock, (void*)(&msg), sizeof(RemoteControlMessage) , 0) < 0)
				{
					printf("Send failed\n");
				}

				usleep(1000 * loadFrequencyInMS);
			}
		}
	}
	
	printf("Exiting client\n");
	shutdown(sock, SHUT_RDWR);
	close(sock);

	return 0;
}